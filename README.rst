CSV to HTML
----------
This is a simple program that takes a delimiter and an input file and outputs it to formatted HTML

``usage: ./csvtohtml [delimiter] [input file] [output].html``
