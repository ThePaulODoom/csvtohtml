BINS = csvtohtml

.PHONY: all clean

all: csvtohtml

csvtohtml: main.c
	$(CC) -Werror -o $@ $<

clean:
	rm $(BINS)
