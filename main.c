#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <stdlib.h>
#include <errno.h>

#define BUFSIZE statbuf.st_size

int main(int argc, char **argv) {
	struct stat statbuf;
	int status;

	if (argc != 4) {
		fprintf(stderr, "usage: %s [delimiter] [input file] [output].html\n", argv[0]);
		exit(1);
	}
	status = stat(argv[2], &statbuf);
	if (status == 0) {
		char* buffer = malloc(BUFSIZE+1);
		memset(buffer, 0, BUFSIZE+1);
		
		int fd = open(argv[2], O_RDONLY);
		if (fd < 1) {
			fprintf(stderr, "Error opening file %s\n", argv[1]);
			fprintf(stderr, "%s\n", strerror(errno));	
			exit(errno);
		}
		read(fd, buffer, BUFSIZE);
		if (errno != 0) {
			fprintf(stderr, "Error reading file %s\n", argv[1]);
			fprintf(stderr, "%s\n", strerror(errno));	
			exit(errno);
		}

		close(fd);

		char* str = buffer;
		char* token;
		char* inner;

		unsigned int count = 0;

		fd = open(argv[3], O_WRONLY | O_CREAT | O_TRUNC, S_IRUSR | S_IWUSR | S_IRGRP | S_IROTH);
		if (fd < 1) {
			fprintf(stderr, "Error opening file %s\n", argv[2]);
			fprintf(stderr, "%s\n", strerror(errno));	
			exit(errno);
		}

		dprintf(fd, 
"<!DOCTYPE html>\n\
<html>\n\
<style>\n\
table, th, td {\n\
border:1px solid black;\n\
}\n\
</style>\n\
<body>\n\
<table style=\"width:100\%\">\n"
				);
		while ((token = strtok_r(str, "\n", &str))) {
			dprintf(fd, "<tr>\n");
			while ((inner = strtok_r(token, argv[1], &token))) {
				if (count == 0) {
					dprintf(fd, "<th>%s</th>\n", inner);
				} else {
					dprintf(fd, "<td>%s</td>\n", inner);
				}
			}	
			dprintf(fd, "</tr>\n");
			count++;
		}
		free(buffer);
		dprintf(fd, "</table>\n</body>\n</html>\n");
		return 0;
	}
	return 1;
}
